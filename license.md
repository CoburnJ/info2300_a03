## Licensing Agreement 

**Subject Matter.** Simple static website based off a tutorial form Microsoft's Azure could based hosting platform 

**Definitions.** Terms and details are defined. 

**Purpose.** The parties are named, with identification, hereinafter called the "Licensor" and "Licensee" or brief names. 

**License.** The licensor allows the licensee to use this product, for about 5 minutes per day, extent in Ontario, and exclusivity for personal use. The licensor doesn't want the licensee to do much with this product other than look at it since it's really from a Microsoft tutorial, so they technically own it. 

**Payment.** Payment to the licensor in Ethereum, royalties are to be paid to the licensor or 100%. Payments are made online every day at midnight. Sales are verified through receipts. Licensor's right to an annual audit and periodic verification of sales. 

**Restrictions.** Licensee cannot do much with this license. Can't sell it or sub-license it or use in certain ways or on certain types of products. 

**Beginning and End of Agreement.** Forever. This agreement never ends, it is bound to one by life, or by an offering of the tears of my enemies. Ownership of this goes back to the licensor at the end.

**Non-disclosure agreement.** Both parties agree not to disclose trade secrets. 

**Non-compete agreement.** Licensor agrees not to allow anyone to compete with the license in the territory and time period designated in the agreement. 

**Jurisdiction.** Ontario. 

**Dispute settlement.** A jousting challenge to show who is rightfully the owner of this project 

**REFERENCED FROM** https://www.thebalancesmb.com/licensing-agreement-that-benefits-both-parties-4153394 AND ASSIGNMENT 1