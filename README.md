# README #

This README is a template used from BitBucket. This will explain some basic information and how to get this asignment 3 static website up and running.

### What is this repository for? ###

Assignment 3, Continuous Integration, static website deployment on Azure to get something live.

### How do I get set up? ###

Simply copy this URL: https://info2300jca03.azurewebsites.net/ and paste into a web browser to view my sample static website.

Alternatively, you can click the link above to go to the static website.

### Who do I talk to? ###

* Jonathan, the repo owner